import React, { useState } from "react"
import Head from 'next/head'
import Image from 'next/image'
import { LoginForm } from "../components/LoginForm";
import { Result } from "../components/EncryptedResult";
import styles from '@/styles/Home.module.css'

export default function Home() {

  const [encryptedData, setEncryptedData] = useState<{
    encryptedUsername: string;
    encryptedPassword: string;
  } | null>(null);

  const [Interaction, setInteraction] = useState(false)
  return (
    <>
      <Head>
        <title>EJE 3 Criptografia - Login con Cifrados</title>
        <meta name="description" content="Inicio de sesión con cifrados simetricos y Asimetricos" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/Areandina.png" />
      </Head>
      <main className={styles.main} style={{ backgroundImage: 'url("/logo3.jpg")' }}>
        <div >
        <div className={`${styles.container} ${Interaction && styles.containerActive} ${encryptedData && styles.containerDecrypt}`}>
            <Image
              className={styles.logo}
              src="/LOGO.png"
              alt="Logo Areandina"
              width={222}
              height={61.7}
              priority
            />
            <LoginForm onSubmit={setEncryptedData} SetInteraction={setInteraction} />
            {encryptedData && (
              <Result
                encryptedUsername={encryptedData.encryptedUsername}
                encryptedPassword={encryptedData.encryptedPassword}
              />
            )}
          </div>

        </div>
      </main>
      <footer>

      </footer>
    </>
  )
}
