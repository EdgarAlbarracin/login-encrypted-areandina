import forge from "node-forge";
import crypto from "crypto-browserify";

const algorithm = "aes-256-cbc";
const symmetricKey = crypto.randomBytes(32); // clave simétrica de 256 bits
const iv = crypto.randomBytes(16); // vector de inicialización de 128 bits

export function encryptSymmetric(text: string): string {
    const cipher = crypto.createCipheriv(algorithm, symmetricKey, iv);
    let encrypted = cipher.update(text, "utf8", "hex");
    encrypted += cipher.final("hex");
    return encrypted;
}

export function generateKeyPair(): { publicKey: string; privateKey: string } {
    const rsa = forge.pki.rsa;
    const keyPair = rsa.generateKeyPair({ bits: 2048, e: 0x10001 });
    const publicKey = forge.pki.publicKeyToPem(keyPair.publicKey);
    const privateKey = forge.pki.privateKeyToPem(keyPair.privateKey);

    return { publicKey, privateKey };
}

export function encryptAsymmetric(text: string, publicKey: string): string {
    const forgePublicKey = forge.pki.publicKeyFromPem(publicKey);
    const buffer = forge.util.createBuffer(text, "utf8");
    const encrypted = forgePublicKey.encrypt(buffer.getBytes(), "RSA-OAEP");
    return forge.util.encode64(encrypted);
}