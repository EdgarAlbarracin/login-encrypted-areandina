import React from "react";
import { AlertTitle, Alert, Box, Typography } from '@mui/material';

type ResultProps = {
  encryptedUsername: string;
  encryptedPassword: string;
};

export const Result: React.FC<ResultProps> = ({
  encryptedUsername,
  encryptedPassword,
}) => {
  return (
    <Box mt={4} sx={{display: 'grid', gridGap:'15px'}}>
      <Typography variant="h5" sx={{color: '#3f3f3f'}}>Resultados encriptados exitosamente</Typography>
      <Alert variant="filled" severity="success">
        <AlertTitle>Usuario encriptado - Cifrado simétrico (AES):</AlertTitle>
        {encryptedUsername}
      </Alert>
      <Alert variant="filled" severity="success">
        <AlertTitle>Contraseña encriptada - Cifrado asimétrico (RSA):</AlertTitle>
        {encryptedPassword}
      </Alert>
    </Box>
  );
};