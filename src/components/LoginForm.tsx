import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import {
    encryptSymmetric,
    generateKeyPair,
    encryptAsymmetric,
} from "../utils/encryption";

type EncryptedData = {
    encryptedUsername: string;
    encryptedPassword: string;
};

type LoginFormProps = {
    onSubmit: (data: EncryptedData) => void;
    SetInteraction: any
};

export const LoginForm: React.FC<LoginFormProps> = ({ onSubmit, SetInteraction }) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [usernameError, setUsernameError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);

    const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setUsername(value);
        setUsernameError(!value.trim());
        SetInteraction(true);
      };
    
      const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.target.value;
        setPassword(value);
        setPasswordError(!value.trim());
        SetInteraction(true);
      };
    

    const handleSubmit = (event: React.FormEvent) => {

        event.preventDefault();

        const isUsernameEmpty = !username.trim();
        const isPasswordEmpty = !password.trim();
    
        setUsernameError(isUsernameEmpty);
        setPasswordError(isPasswordEmpty);
        SetInteraction(true);
        if (isUsernameEmpty || isPasswordEmpty) {
          return;
        }

        const encryptedUsername = encryptSymmetric(username);
        const keyPair = generateKeyPair();
        const encryptedPassword = encryptAsymmetric(password, keyPair.publicKey);

        onSubmit({ encryptedUsername, encryptedPassword });
    };

    return (

    <form onSubmit={handleSubmit} style={{width: '100%'}}>
      <Stack spacing={2}>
        <TextField
          label="Usuario"
          variant="outlined"
          value={username}
          onChange={handleUsernameChange}
          inputProps={{ autoComplete: "off" }}
          error={usernameError}
          helperText={usernameError && "El campo Usuario no puede estar vacío"}
        />
        <TextField
          label="Contraseña"
          type="password"
          variant="outlined"
          value={password}
          onChange={handlePasswordChange}
          inputProps={{ autoComplete: "off" }}
          error={passwordError}
          helperText={passwordError && "El campo Contraseña no puede estar vacío"}
        />
        <Button variant="contained" type="submit" style={{ margin: '40px 0 0', padding: '12px 0'}}>
          Iniciar sesión
        </Button>
      </Stack>
        </form>
    );
};
